package ru.sfu.jmsService.comp;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import ru.sfu.jmsService.message.Message;

@Component
public class Receiver {
    private String myemail = "ivan.materov23@gmail.com";

    @RabbitListener(queues = "perfumery-queue", containerFactory = "rabbitListenerContainerFactory")
    public void listen(Message message) {
        System.out.println("You need to send " + message.getMessage() + " to " +
                message.getPerfumery().toString());
    }

}
