package ru.sfu.jmsService.model;

public class Perfumery {
    private Long id;

    private Double volume;

    private Integer price;

    private String producer;

    private String collection;

    private String title;

    public Perfumery() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Perfumery(Long id, Double volume, Integer price, String producer, String collection, String title) {
        this.id = id;
        this.volume = volume;
        this.price = price;
        this.producer = producer;
        this.collection = collection;
        this.title = title;
    }
}
