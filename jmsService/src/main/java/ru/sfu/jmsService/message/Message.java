package ru.sfu.jmsService.message;

import ru.sfu.jmsService.model.Perfumery;

import java.io.Serializable;

public class Message implements Serializable {
    private String message;
    private Perfumery perfumery;

    public Message(String message, Perfumery perfumery) {
        this.message = message;
        this.perfumery = perfumery;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Perfumery getPerfumery() {
        return perfumery;
    }

    public void setPerfumery(Perfumery perfumery) {
        this.perfumery = perfumery;
    }
}

