package ru.sfu.mvc.message;

import ru.sfu.mvc.model.Perfumery;

import java.io.Serializable;

public class Message implements Serializable {
    private String message;
    private Perfumery perfumery;

    public Message() {
    }

    public Message(String message, Perfumery perfumery) {
        this.message = message;
        this.perfumery = perfumery;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Perfumery getPerfumery() {
        return perfumery;
    }

    public void setPerfumery(Perfumery perfumery) {
        this.perfumery = perfumery;
    }
}

