package ru.sfu.mvc.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sfu.mvc.model.Perfumery;

import java.util.List;

@Repository
public interface PerfumeryRepository extends JpaRepository<Perfumery, Long> {
    List<Perfumery> findByPrice(Integer price);
    List<Perfumery> findByProducer(String producer);
    List<Perfumery> findByCollection(String collection);
    List<Perfumery> findByTitle(String title);
}
