package ru.sfu.mvc.model;

import lombok.*;

import javax.persistence.*;

@Entity
@ToString
@Getter
@Setter
@NoArgsConstructor
@Table(name="perfumery")
public class Perfumery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double volume;

    private Integer price;

    private String producer;

    private String collection;

    private String title;

    public Perfumery(Double volume, Integer price, String producer, String collection, String title) {
        this.volume = volume;
        this.price = price;
        this.producer = producer;
        this.collection = collection;
        this.title = title;
    }
}
